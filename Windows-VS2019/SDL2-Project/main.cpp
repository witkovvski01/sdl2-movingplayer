/*
  Moving the player with keys, velocity based.   
*/

//For exit()
#include <stdlib.h>

//For printf()
#include <stdio.h>

// For round()
#include <math.h>

#if defined(_WIN32) || defined(_WIN64)
    //The SDL library
    #include "SDL.h"
    //Support for loading different types of images.
    #include "SDL_image.h"
#else
    #include "SDL2/SDL.h"
    #include "SDL2/SDL_image.h"
#endif

const int WINDOW_WIDTH = 800;
const int WINDOW_HEIGHT= 600;

const int SDL_OK = 0;

int main( int argc, char* args[] )
{
    // Declare window and renderer objects
    SDL_Window*	     gameWindow = nullptr;
    SDL_Renderer*    gameRenderer = nullptr;

    // Temporary surface used while loading the image
    SDL_Surface*     temp = nullptr;

    // Texture which stores the actual sprite (this
    // will be optimised).
    SDL_Texture*     backgroundTexture = nullptr;
    SDL_Texture*     playerTexture = nullptr;


    // Player variables
    const int PLAYER_SPRITE_WIDTH = 64;
    const int PLAYER_SPRITE_HEIGHT = 64;
    SDL_Rect standRight; // first sprite of the sheet
    SDL_Rect targetRectangle;
    float playerSpeed = 50.0f;
    float playerX = 250.0f;
    float playerY = 250.0f;
    
    // Window control 
    SDL_Event event;
    bool quit = false;  //false

    //Input - keys/joysticks?
    float verticalInput = 0.0f; 
    float horizontalInput = 0.0f; 
    
    // Keyboard
    const Uint8 *keyStates;
    
    // Timing variables
    unsigned int currentTimeIndex;
    unsigned int prevTimeIndex; 
    unsigned int timeDelta;
    float timeDeltaInSeconds;

    // SDL allows us to choose which SDL components are going to be
    // initialised. We'll go for everything for now!
    int sdl_status = SDL_Init(SDL_INIT_EVERYTHING);

    if(sdl_status != SDL_OK)
    {
        //SDL did not initialise, report and error and exit. 
        printf("Error -  SDL Initialisation Failed\n");
        exit(1);
    }

    gameWindow = SDL_CreateWindow("Hello CIS4008",   // Window title
                            SDL_WINDOWPOS_UNDEFINED, // X position
                            SDL_WINDOWPOS_UNDEFINED, // Y position
                            WINDOW_WIDTH,            // width
                            WINDOW_HEIGHT,           // height               
                            SDL_WINDOW_SHOWN);       // Window flags

   
    
    if(gameWindow != nullptr)
    {
        // if the window creation succeeded create our renderer
        gameRenderer = SDL_CreateRenderer(gameWindow, 0, 0);

        if(gameRenderer == nullptr)
        {
          printf("Error - SDL could not create renderer\n");
          exit(1);
        }
    }
    else
    {
        // could not create the window, so don't try and create the renderer. 
        printf("Error - SDL could not create Window\n");
        exit(1);
    }
    
     // Track Keystates array
	keyStates = SDL_GetKeyboardState(NULL);
      
    /**********************************
     *    Setup background image     *
     * ********************************/

    // Load the sprite to our temp surface
    temp = IMG_Load("assets/images/background.png");

    if(temp == nullptr)
    {
        printf("Background image not found!");
    }    

    // Create a texture object from the loaded image
    // - we need the renderer we're going to use to draw this as well!
    // - this provides information about the target format to aid optimisation.
    backgroundTexture = SDL_CreateTextureFromSurface(gameRenderer, temp);

    // Clean-up - we're done with 'image' now our texture has been created
    SDL_FreeSurface(temp);
    temp = nullptr;


    /**********************************
     *    Setup walker image     *
     * ********************************/

    // Load the sprite to our temp surface
    temp = IMG_Load("assets/images/walker1.png");

    if(temp == nullptr)
    {
        printf("Walker image not found!");
    }

    // Create a texture object from the loaded image
    // - we need the renderer we're going to use to draw this as well!
    // - this provides information about the target format to aid optimisation.
    playerTexture = SDL_CreateTextureFromSurface(gameRenderer, temp);

    // Clean-up - we're done with 'temp' now our texture has been created
    SDL_FreeSurface(temp);
    temp = nullptr;

    //Setup source and destination rects
    targetRectangle.x = 0; //can't use these to track position
    targetRectangle.y = 0; //due to small position changes
    targetRectangle.w = PLAYER_SPRITE_WIDTH;
    targetRectangle.h = PLAYER_SPRITE_HEIGHT;

    standRight.x = 2*PLAYER_SPRITE_WIDTH;
    standRight.y = 2*PLAYER_SPRITE_HEIGHT;
    standRight.w = PLAYER_SPRITE_WIDTH;
    standRight.h = PLAYER_SPRITE_HEIGHT;

    prevTimeIndex = SDL_GetTicks();

    // Game loop
    while(!quit) // while quit is not true
    { 
	    // Calculate time elapsed
        // Better approaches to this exist - https://gafferongames.com/post/fix_your_timestep/

        currentTimeIndex = SDL_GetTicks();	
        timeDelta = currentTimeIndex - prevTimeIndex; //time in milliseconds
        timeDeltaInSeconds = timeDelta * 0.001;
        	
        // Store current time index into prevTimeIndex for next frame
        prevTimeIndex = currentTimeIndex;

        // Handle input 

        if( SDL_PollEvent( &event ))  // test for events
        { 
            switch(event.type) 
            { 
                case SDL_QUIT:
    		        quit = true;
    		    break;

                // Key pressed event
                case SDL_KEYDOWN:
                    switch (event.key.keysym.sym)
                    {
                    case SDLK_ESCAPE:
                        quit = true;
                        break;
                    }
                break;

                // Key released event
                case SDL_KEYUP:
                    switch (event.key.keysym.sym)
                    {
                    case SDLK_ESCAPE:
                        //  Nothing to do here.
                        break;
                    }
                break;
                
                default:
                    // not an error, there's lots we don't handle. 
                    break;    
            }
        }

        

         // This could be more complex, e.g. increasing the vertical 
         // input while the key is held down. 
        if (keyStates[SDL_SCANCODE_UP]) 
        {
            verticalInput = -1.0f;
        }
        else 
        {
            verticalInput = 0.0f;
        }

         // Update Game World

	    // Calculate player velocity. 
	    // Note: This is imperfect, no account taken of diagonal!
        float xVelocity = verticalInput * playerSpeed;
        float yVelocity = horizontalInput * playerSpeed;

        // Calculate distance travelled since last update
        float yMovement = timeDeltaInSeconds * xVelocity;
        float xMovement = timeDeltaInSeconds * yVelocity;

        // Update player position.
	    playerX += xMovement;
        playerY += yMovement;

        // Move sprite to nearest pixel location.
	    targetRectangle.y = round(playerY);
	    targetRectangle.x = round(playerX);


        //Draw stuff here.

        // 1. Clear the screen
        SDL_SetRenderDrawColor(gameRenderer, 0, 0, 0, 255);
        // Colour provided as red, green, blue and alpha (transparency) values (ie. RGBA)

        SDL_RenderClear(gameRenderer);

        // 2. Draw the scene
        SDL_RenderCopy(gameRenderer, backgroundTexture, NULL, NULL);

        SDL_RenderCopy(gameRenderer, playerTexture, &standRight, &targetRectangle);

        // 3. Present the current frame to the screen
        SDL_RenderPresent(gameRenderer);

    }

    //Clean up!
    SDL_DestroyTexture(playerTexture);
    playerTexture = nullptr;   
   
    SDL_DestroyTexture(backgroundTexture);
    backgroundTexture = nullptr;
    
    SDL_DestroyRenderer(gameRenderer);
    gameRenderer = nullptr;
    
    SDL_DestroyWindow(gameWindow);
    gameWindow = nullptr;   

    //Shutdown SDL - clear up resources etc.
    SDL_Quit();

    return 0;
}
